#include "../TrigEgammaForwardFastCaloHypoAlg.h"
#include "../TrigEgammaForwardFastCaloHypoToolInc.h"
#include "../TrigEgammaForwardPrecisionCaloHypoAlgMT.h"
#include "../TrigEgammaForwardPrecisionCaloHypoToolInc.h"
#include "../TrigEgammaForwardPrecisionElectronHypoAlgMT.h"
#include "../TrigEgammaForwardPrecisionElectronHypoToolInc.h"




DECLARE_COMPONENT( TrigEgammaForwardFastCaloHypoAlg )
DECLARE_COMPONENT( TrigEgammaForwardFastCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaForwardPrecisionCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaForwardPrecisionCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaForwardPrecisionElectronHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaForwardPrecisionElectronHypoToolInc )

